---
title: Canonical URL - Testing LangAbsURL
hide_sidebar: true
canonical_url: ./params
---

This page tests whether or not `absLangURL` gets applied correctly to `canonical_url` param.

The `canonical_url` is set to `./params`.

The `href` value of the `<link rel="canonical" ...>` element should be `"//<domain>/fr/params"`.