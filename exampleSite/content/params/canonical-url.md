---
title: Canonical URL
hide_sidebar: true
canonical_url: https://google.com
---

The `canonical_url` config parameter sets the canonical url of the page. The value must be an *absolute url*. 

If `canonical_url` is not specified, the canonical URL will be the pages' default url.

```md
title: Canonical URL
hide_sidebar: true
canonical_url: https://google.com
```